extends RigidBody

const MASS: float = 1.8
var thrust = Vector3(0, 0, 0)
var ang = Vector3.ZERO

const MAX_THRUST: float = 5.0
const MAX_ANG: float = 2.0
const ANG_ACCEL: float = 1.0

func _ready():
	mass = MASS

# Add the vector `delta` to the vector `thrust`
func update_thrust(delta: Vector3):
	thrust.x = clamp(thrust.x + delta.x, -MAX_THRUST, MAX_THRUST)
	thrust.y = clamp(thrust.y + delta.y, -MAX_THRUST, MAX_THRUST)
	thrust.z = clamp(thrust.z + delta.z, -MAX_THRUST, MAX_THRUST)

func _integrate_forces(state):
	if Input.is_action_pressed("ui_up"):
		update_thrust(Vector3(1, 0, 0))
	elif Input.is_action_pressed("ui_down"):
		update_thrust(Vector3(-1, 0, 0))
	elif Input.is_action_pressed("ui_right"):
		state.angular_velocity += Vector3.RIGHT
	elif Input.is_action_pressed("ui_left"):
		state.angular_velocity += Vector3.LEFT
	state.linear_velocity = global_transform.basis.xform(thrust)
