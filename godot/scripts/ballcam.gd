extends Camera

export (NodePath) var pivot_node_path = null
export (NodePath) var target_node_path = null

export var offset_distance: float = 4.5
export var offset_height: float = 2.0

var pivot_node: Node
var target_node: Node

func _ready():
	pivot_node = get_node(pivot_node_path)
	target_node = get_node(target_node_path)

func _physics_process(delta):
	var pivot = pivot_node.global_transform.origin
	var offset = Vector3(0, offset_height, offset_distance)
	global_transform = Transform(Basis.IDENTITY, pivot).looking_at(target_node.global_transform.origin, Vector3.UP).translated(offset)
